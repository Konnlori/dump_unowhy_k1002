#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:13208480:cbc822291cef24edef87a5bcfc356e7c9f54eb7f; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:7971744:a8be0b0c0819e88b55e0740dd8210b77c382407c EMMC:/dev/block/platform/bootdevice/by-name/recovery cbc822291cef24edef87a5bcfc356e7c9f54eb7f 13208480 a8be0b0c0819e88b55e0740dd8210b77c382407c:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
