## full_unowhy_k1002-user 9 PPR1.180610.011 user.xmlyz.1564163634 release-keys
- Manufacturer: malata
- Platform: mt8167
- Codename: Y10G001S4M
- Brand: UNOWHY
- Flavor: full_unowhy_k1002-user
- Release Version: 9
- Id: PPR1.180610.011
- Incremental: 1564163634
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: fr-FR
- Screen Density: 160
- Fingerprint: UNOWHY/Y10G001S4M_EEA/Y10G001S4M:9/PPR1.180610.011/1564163634:user/release-keys
- OTA version: 
- Branch: full_unowhy_k1002-user-9-PPR1.180610.011-user.xmlyz.1564163634-release-keys
- Repo: unowhy_y10g001s4m_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
